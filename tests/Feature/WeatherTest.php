<?php

it('returns a successful status 200', function () {
    $response = $this->get('/');

    $response->assertStatus(200);
});

test('go to /locale/(fr|en) redirect to homepage with status 302', function () {
    $locale = "en";
    $response = $this->get("/locale/{$locale}");
    $currentLocale = session()->get("locale");

    expect($currentLocale)->toBe($locale);
    $response->assertRedirect("/");
    $response->assertStatus(302);
});

test('display view data by city', function () {
    $cityName = "Marseille";
    $response = $this->post("/city-name/{$cityName}");

    $response->assertViewHas("cities");
    $response->assertViewHas("iconName");
    $response->assertViewHas("date");
    $response->assertViewHas("precipitation");
    $response->assertViewHas("cityName", $cityName);
    $response->assertStatus(200);
});
