<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LocaleSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $currentLocale = $request->session()->get("locale", "fr");

        app()->setLocale($currentLocale);

        return $next($request);
    }
}
