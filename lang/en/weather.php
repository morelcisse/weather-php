<?php

return [

    "precipitation" => "Precipitation",
    "humidity" => "Humidity",
    "wind" => "Wind",
    "use_your_api_key" => "Use your API key",
    "api_key" => "API key",
    "use_default_api_key" => "Use old",
    "api_limit" => "This application uses an external service with a free plan and therefore with limited calls. If you see this message, it means that the number of calls to this API has been reached. Use the field just above to use your key directly. Create an account here https://openweathermap.org/ for the occasion, you can delete it after your tests.",
    /* -------------------------------------------------------------------------- */
    /*                                 DAY OF WEEK                                */
    /* -------------------------------------------------------------------------- */
    "sunday" => "Sunday",
    "monday" => "Monday",
    "tuesday" => "Tuesday",
    "wednesday" => "Wednesday",
    "thursday" => "Thursday",
    "friday" => "Friday",
    "saturday" => "Saturday",
    "sunday_abbr" => "Sun.",
    "monday_abbr" => "Mon.",
    "tuesday_abbr" => "Tue.",
    "wednesday_abbr" => "Wed.",
    "thursday_abbr" => "Thur.",
    "friday_abbr" => "Fri.",
    "saturday_abbr" => "Sat.",
    /* -------------------------------------------------------------------------- */
    /*                                   MONTHS                                   */
    /* -------------------------------------------------------------------------- */
    "january" => "January",
    "february" => "February",
    "march" => "March",
    "april" => "April",
    "may" => "May",
    "june" => "June",
    "july" => "July",
    "august" => "August",
    "september" => "September",
    "october" => "October",
    "november" => "November",
    "december" => "December",
    "january_abbr" => "Jan.",
    "february_abbr" => "Feb.",
    "march_abbr" => "Mar.",
    "april_abbr" => "Apr.",
    "may_abbr" => "May",
    "june_abbr" => "Jun.",
    "july_abbr" => "Jul.",
    "august_abbr" => "Aug.",
    "september_abbr" => "Sep.",
    "october_abbr" => "Oct.",
    "november_abbr" => "Nov.",
    "december_abbr" => "Dec.",

];
